package org.orcateam.tcbmce.engine;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.mapping.MappingException;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.orcateam.tcbmce.exception.TCMBCurrencyExporterException;
import org.orcateam.tcbmce.modal.Currencies;
import org.orcateam.tcbmce.modal.Currency;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TCMBCurrencyEngine {

    public Map<String, Currency> getCurrencyValuesFromTCMB() throws TCMBCurrencyExporterException {
        HttpResponse response = null;
        try {
            response = makeRequest();
        } catch (IOException e) {
            e.printStackTrace();
            throw new TCMBCurrencyExporterException(e);
        }
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            InputStream is = null;
            try {
                is = entity.getContent();
            } catch (IOException e) {
                e.printStackTrace();
                throw new TCMBCurrencyExporterException(e);
            }
            List<Currency> currencyList = null;
            try {
                currencyList = parseInputStreamToList(is);
            } catch (IOException e) {
                e.printStackTrace();
                throw new TCMBCurrencyExporterException(e);
            } catch (MappingException e) {
                e.printStackTrace();
                throw new TCMBCurrencyExporterException(e);
            } catch (MarshalException e) {
                e.printStackTrace();
                throw new TCMBCurrencyExporterException(e);
            } catch (ValidationException e) {
                e.printStackTrace();
                throw new TCMBCurrencyExporterException(e);
            }
            return getListAsMap(currencyList);
        } else {
            return null;
        }
    }

    public Currency getCurrencyByCode(String code) throws TCMBCurrencyExporterException {
        return getCurrencyValuesFromTCMB().get(code);
    }

    private HttpResponse makeRequest() throws IOException {
        HttpGet httpGet = new HttpGet("http://www.tcmb.gov.tr/kurlar/today.xml");
        httpGet.setHeader("charset", "UTF-8");

        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpResponse response = httpclient.execute(httpGet);
        response.setLocale(new Locale("tr", "TR"));
        return response;
    }

    private List<Currency> parseInputStreamToList(InputStream is) throws IOException, MappingException, MarshalException, ValidationException {
        InputStreamReader reader = new InputStreamReader(is);

        Mapping mapping = new Mapping();
        mapping.loadMapping(this.getClass().getResource("/mapping.xml"));

        Unmarshaller unmarshaller = new Unmarshaller(Currencies.class);
        unmarshaller.setMapping(mapping);

        Currencies currencies = (Currencies) unmarshaller.unmarshal(reader);
        List<Currency> currencyList = currencies.getCurrencyList();
        return currencyList;
    }

    private Map<String, Currency> getListAsMap(List<Currency> currencyList) {
        Map<String, Currency> currencyCodeMap = new HashMap<String, Currency>();
        for (Currency currency : currencyList) {
            currencyCodeMap.put(currency.getCurrencyCode(), currency);
        }
        return currencyCodeMap;
    }

}
