package org.orcateam.tcbmce.exception;


public class TCMBCurrencyExporterException extends Exception {

    public TCMBCurrencyExporterException(String s) {
        super(s);
    }

    public TCMBCurrencyExporterException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TCMBCurrencyExporterException(Throwable throwable) {
        super(throwable);
    }
}
