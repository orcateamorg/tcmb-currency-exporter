package org.orcateam.tcbmce.modal;

public class Currency {

    private String currencyCode;
    private Integer unit;
    private String name;
    private String turkishName;
    private Double buyPrice;
    private Double sellPrice;
    private Double forexBuyPrice;
    private Double forexSellPrice;
    private Double crossRateUSD;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTurkishName() {
        return turkishName;
    }

    public void setTurkishName(String turkishName) {
        this.turkishName = turkishName;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Double getForexBuyPrice() {
        return forexBuyPrice;
    }

    public void setForexBuyPrice(Double forexBuyPrice) {
        this.forexBuyPrice = forexBuyPrice;
    }

    public Double getForexSellPrice() {
        return forexSellPrice;
    }

    public void setForexSellPrice(Double forexSellPrice) {
        this.forexSellPrice = forexSellPrice;
    }

    public Double getCrossRateUSD() {
        return crossRateUSD;
    }

    public void setCrossRateUSD(Double crossRateUSD) {
        this.crossRateUSD = crossRateUSD;
    }
}
