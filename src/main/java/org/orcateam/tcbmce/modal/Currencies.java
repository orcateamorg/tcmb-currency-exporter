package org.orcateam.tcbmce.modal;

import java.util.Vector;

public class Currencies {

    private Vector currencyList = new Vector();

    public Vector getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(Vector currencyList) {
        this.currencyList = currencyList;
    }
}
