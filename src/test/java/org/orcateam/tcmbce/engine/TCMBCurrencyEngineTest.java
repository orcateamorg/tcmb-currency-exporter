package org.orcateam.tcmbce.engine;

import org.junit.Assert;
import org.junit.Test;
import org.orcateam.tcbmce.engine.TCMBCurrencyEngine;
import org.orcateam.tcbmce.modal.Currency;

import java.util.Map;

public class TCMBCurrencyEngineTest {

    @Test
    public void testCurrencyValuesFromTCMB() throws Exception {
        TCMBCurrencyEngine TCMBCurrencyEngine = new TCMBCurrencyEngine();
        Map<String, Currency> currencies = TCMBCurrencyEngine.getCurrencyValuesFromTCMB();
        for (Map.Entry<String, Currency> entrySet : currencies.entrySet()) {
            Assert.assertEquals(entrySet.getKey(), entrySet.getValue().getCurrencyCode());
            System.out.println(entrySet.getKey() + ":" + entrySet.getValue().getBuyPrice());
        }
    }

    @Test
    public void testCurrencyByCode() throws Exception {
        TCMBCurrencyEngine TCMBCurrencyEngine = new TCMBCurrencyEngine();
        Currency currency = TCMBCurrencyEngine.getCurrencyByCode("USD");
        Assert.assertNotNull(currency);
        System.out.println(currency.getCurrencyCode() + ":" + currency.getBuyPrice());
    }

}
